var i18next = require('i18next').default;

var BaseView = require("components/commons/BaseView");

var LanguageSelectorModel = require("./LanguageSelectorModel");

require("./footer.less");
module.exports = BaseView.extend({
    template : require("./footer.hbs"),
    el: '#app',
    generatedEl : null,
    initialize: function (options) {
        BaseView.prototype.initialize.apply(this, arguments);
        this.cssClass = "footer";
        if (options.cssClass) this.cssClass = this.cssClass + " " + options.cssClass;
        this.elementID = options && options.attributes && options.attributes.id || null;
        this.languageSelectorModel = new LanguageSelectorModel();
        this.listenTo(this.languageSelectorModel, "change", this.render, this);
    },
    preRender : function () {
      if (this.generatedEl) {
        this.generatedEl.remove();
        this.generatedEl = null;
      }
    },
    contentRender: function () {
        var currentLng = i18next.language;
        this.generatedEl = $(this.template({
          baseURL : this.baseURL,
          lang: currentLng,
          baseImageURL : this.baseImageURL,
          url : {
            es : this.languageSelectorModel.getURLByLanguage("es"),
            en : this.languageSelectorModel.getURLByLanguage("en"),
            pt : this.languageSelectorModel.getURLByLanguage("pt"),
            "help" : $("meta[name='_help_url']").attr("content"),
            "about-us" : $("meta[name='_about_us_url']").attr("content"),
            "terms-of-use" : $("meta[name='_terms_and_conditions_url']").attr("content"),
            "contact" : $("meta[name='_contact_url']").attr("content"),
            "how-it-works" : $("meta[name='_how_it_works']").attr("content"),
            "facebook": $("meta[name='_facebook_url']").attr("content"),
            "twitter": $("meta[name='_twitter_url']").attr("content"),
            "instagram": $("meta[name='_instagram_url']").attr("content")
          }
        }));
        this.generatedEl.addClass(this.cssClass);
        this.generatedEl.attr("id", this.elementID);
        this.$el.after(this.generatedEl);
        this.collapseFooter();
    },
    setLanguageSelectorURLs : function(urls) {
        this.languageSelectorModel.set(urls);
    },
    collapseFooter: function() {
      $('#collapsableFooter').on('shown.bs.collapse', function () {
        $('html, body').animate({ scrollTop: $(document).height() }, "slow");
        $('.collapse-footer').find('.glyphicon-menu-up')
          .removeClass('glyphicon-menu-up')
          .addClass('glyphicon-menu-down');
      })

      $('#collapsableFooter').on('hidden.bs.collapse', function () {
        $('.collapse-footer').find('.glyphicon-menu-down')
          .removeClass('glyphicon-menu-down')
          .addClass('glyphicon-menu-up');
      })
    }
});
